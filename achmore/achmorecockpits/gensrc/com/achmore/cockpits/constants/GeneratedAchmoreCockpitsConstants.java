/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 6 Dec, 2018 11:27:42 AM                     ---
 * ----------------------------------------------------------------
 */
package com.achmore.cockpits.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAchmoreCockpitsConstants
{
	public static final String EXTENSIONNAME = "achmorecockpits";
	
	protected GeneratedAchmoreCockpitsConstants()
	{
		// private constructor
	}
	
	
}
