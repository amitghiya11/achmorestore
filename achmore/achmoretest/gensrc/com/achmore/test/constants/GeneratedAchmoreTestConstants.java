/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 6 Dec, 2018 11:27:42 AM                     ---
 * ----------------------------------------------------------------
 */
package com.achmore.test.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAchmoreTestConstants
{
	public static final String EXTENSIONNAME = "achmoretest";
	
	protected GeneratedAchmoreTestConstants()
	{
		// private constructor
	}
	
	
}
